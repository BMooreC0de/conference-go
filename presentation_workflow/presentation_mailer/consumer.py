import json
import pika
from pika.exceptions import AMQPConnectionError 
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    contact = json.loads(body)
    send_mail(
        recipient_list=[contact["presenter_email"]],
        from_email="admin@conference.go",
        subject="Your presentation has been accepted",
        message=f"{contact['presenter_name']}, we're happy to tell you that your presentation {contact['title']} has been accepted",
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    var_name = json.loads(body)
    send_mail(
        recipient_list=[var_name["presenter_email"]],
        from_email="admin@conference.go",
        subject="Your presentation has been rejected",
        message=f"{var_name['presenter_name']}, we're sad to tell you that your presentation {var_name['title']} has been rejected",
        fail_silently=False,
    )
# Create a main method to run
def main():
    #This while loop will try to make a connection
    while True:
        try:
            # Set the hostname that we'll connect to
            parameters = pika.ConnectionParameters(host="rabbitmq")
            # Create a connection to RabbitMQ
            connection = pika.BlockingConnection(parameters)
            # Open a channel to RabbitMQ
            channel = connection.channel()
            # Create a queue if it does not exist
            channel.queue_declare(queue="presentation_approvals")
            channel.queue_declare(queue="presentation_rejections")
            # Configure the consumer to call the process_message function
            # when a message arrives
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            # Print a status
            print(" [*] Waiting for messages. To exit press CTRL+C")
            # Tell RabbitMQ that you're ready to receive messages
            channel.start_consuming()
        #If the connection fails, then it will print out a message and go to sleep for 2 seconds and it will execute again.
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)

    
# Just extra stuff to do when the script runs
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)